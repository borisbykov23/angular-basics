export class Ingredient {
  // public v konstruktoru znamena ze se ty argumenty vytvori sami
  constructor(public name: string, public amount: number) {

  }
}
