import {Component, OnInit, EventEmitter, Output} from '@angular/core';
import {Recipe} from '../recipe.model';



@Component({
  selector: 'app-recipe-list',
  templateUrl: './recipe-list.component.html',
  styleUrls: ['./recipe-list.component.css']
})
export class RecipeListComponent implements OnInit {
  @Output() recipeWasSelected = new EventEmitter<Recipe>();

  recipes: Recipe[] = [
    new Recipe('Test recipe', 'description', 'https://www.brnoexpatcentre.eu/wp-content/uploads/povidla.jpg'),
    new Recipe('Hmmm test recipe', 'description', 'https://www.brnoexpatcentre.eu/wp-content/uploads/povidla.jpg')
  ];

  constructor() { }

  ngOnInit(): void {
  }

  onRecipeSelected(recipeElement: Recipe): void {
    this.recipeWasSelected.emit(recipeElement);
  }

}
